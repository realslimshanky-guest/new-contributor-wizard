# New Contributor Wizard - Contributors

Contributors to this project can be seen [here](https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard/graphs/master).

#### Maintainer(s)

- [Shashank Kumar](https://shanky.xyz) - Starting GSoC 2018 as The Debian's [Project](https://summerofcode.withgoogle.com/projects/#5056989357408256)

#### Special Mention

- [Daniel Pocock](https://danielpocock.com/) - Project Idea and Mentor. Daniel came up with the brainchild which led to the formation of this application and has been guiding since the beginning to help shape the project for the betterment of the community.
  
- [Sanyam Khurana](https://sanyamkhurana.com) - Mentor and Code Review. Sanyam with his expertise in Python has been reviewing all the merge requests made to this application in order for the code to follow the standards and practices and developer friendly.

- [Akshay Arora](https://github.com/akshayaurora) - Core Kivy Contributor. Akshay has been a long time Kivy developer and contributor and has been helping out with the features Kivy has to offer in order to scale the application.

- [Laurens](https://www.lvh.io/about.html) - Author Crypt 101. Laurens' book on cryptography has become a reference for the Encryption modules in order to develop tools and tutorials.
