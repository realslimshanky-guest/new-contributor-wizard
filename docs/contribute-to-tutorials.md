# New Contributor Wizard - Contribute To Tutorials

Contributing Tutorials is as simple as creating a JSON file, NO CODING REQUIRED! You can create a JSON file in order to contribute to Tutorials of any module of your choice. Here's a step by step guide which will help you out.

## Step 1 - Installing The Application

Make sure you follow [Developer](developer.md) docs to build the application from the source on your local machine. Once you are setup and application works properly move to the next step.

## Step 2 - Updating Tutorial Index

The way the project is structured there are two places where you'll have to either modify a JSON file or add a new JSON file to create Tutorial. They are:

- `data/module_data.json` is the file which acts as the index of all the Tutorials present inside the application. You'll have to first mention your tutorial here. For example, if you want to add a tutorial to Encryption module, you first find out the entry for `encryption` key (i.e. the module name itself in lower case) which looks like

![step2-contribute-to-tutorials](https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard/uploads/0e49d5a7292de8c7e7af1397ec92b9e7/step2-contribute-to-tutorials.mp4)

```json
{"encryption":
    {
        "tutorials": [
            {
                "title": "Encryption 101",
                "difficulty": "Beginner"
            }
        ],
        "tools": [
            {
                "title": "Display and manage key pair",
                "difficulty": "Beginner"
            },
            {
                "title": "Create key pair",
                "difficulty": "Beginner"
            },
            {
                "title": "Encrypt a message",
                "difficulty": "Intermediate"
            },
            {
                "title": "Decrypt a message",
                "difficulty": "Intermediate"
            }
        ]
    }
}
```

Here, you can see there's already a Tutorial listed. Similarly, list your own tutorial. In the like this

```json
{"encryption":
    {
        "tutorials": [
            {
                "title": "Encryption 101",
                "difficulty": "Beginner"
            },
            {
                "title": "My Awesome Tutorial",
                "difficulty": "Beginner"
            }
        ],
        ...
```

So, your tutorial should be listed using `{}` with two compulsory key value pair. First, `title` key, whose value should be the Title of your Tutorial. And second, `difficulty` key, whose value should be set to either `Begineer`, `Intermediate` or `Advance`. This is the only requirement in order to list your tutorial.

Once you have listed, open the module inside the application, say `Encryption` going by the above example and click on `Tutorials` from the bottom navigation bar. You'll see your tutorial listed. If you try to open the application the application will break, that's a good thing and a sign that you still have to add Tutorial JSON file which we'll see how to in later steps.

![Encryption Tutorials Menu](https://i.imgur.com/1DulVxX.png)

## Step 3 - Finding Folder TO Save Tutorial JSON

After listing your tutorial, it's now time to create your Tutorial JSON. Let's say you want to build Tutorial for `Encryption` module and you have listed your tutorial under `encryption` key on `data/module_data.json` following Step 2. Now, you'll have to find the correct folder to put your JSON file (which we'll be creating in later step). Below is a table to help you out.

| Module in which you want to add your Tutorial | Folder to add your Tutorial JSON file |
| --------------------------------------- | -------------------------------- |
| Blog | `modules/course_modules/blog/tutorials/` |
| CLI | `modules/course_modules/cli/tutorials/` |
| Communication | `modules/course_modules/communication/tutorials/` |
| Encryption | `modules/course_modules/encryption/tutorials/` |
| How To Use | `modules/course_modules/how_to_use/tutorials/` |
| VCS | `modules/course_modules/vcs/tutorials/` |
| Way Ahead | `modules/course_modules/way_ahead/tutorials/` |

## Step 4 - Creating Tutorial JSON

In the previous step, you found out what is the correct location to put Tutorial JSON file. Now let's create a JSON file in that folder. Name of the JSON file should be carefully choosen which should follow the below guidelines in order for everything to work correctly.

1. Name of the Tutorial JSON file should be in lowercase of the title given to the Tutorial in `data/module_data.json` in Step 2

2. Spaces in the Name should be replaced by underscore `_`

3. Add `.json` at the end of the filename

For example, if the Tutorial's Title given in the `data/module_data.json` is `My Awesome Tutorial` then the file name of Tutorial JSON should be only and only `my_awesome_tutorial.json`. Also, add the following content to the file to make it a valid JSON.

```json
[
]
```

Now try to `Open` `My Awesome Tutorial` from the Tutorial Menu as seen above. What you see is a blank Tutorial. This means our JSON file works and now we head to add content to tutorial.

![My Awesome Tutorial](https://i.imgur.com/S5EDjcR.png)

## Step 5 - Updating The Tutorial JSON

Each Tutorial JSON file is a array of `{}` obejcts. And each of those object must contain one and only one key i.e. `lesson` and value of this key should also be a `[]` object.

Each `{}` with a `lesson` represents a lesson in tutorial. Copy the code below and replace it with what you have in your Tutorial JSON file, restart the application and open your tutorial again.

```json
[
    {
        "lesson": []
    },
    {
        "lesson": []
    },
    {
        "lesson": []
    }
]
```

We can see there are 3 different Lessons without any content and there's a handy navigation to switch between any of them. Cool right? Your first `lesson` becomes Lesson 1, second `lesson` becomes Lesson 2 and so on.

Now when you are creating a Tutorial, you would like to add some content inside your Lesson, that's what we define in `[]`.

## Step 6 - Content Supported In Tutorial JSON

Here comes the cool part, adding content to your tutorial. Everything which you want to add in a Lesson should go into the respective `[]`. There are 4 type of content you can add in your Lesson, they are

### 1. **Text**

A simple Text can be added to Lesson by using the following code

```json
[
    {
        "lesson": [
            {
                "type": "text",
                "content": "This is a text"
            }
        ]
    },
    ...
```

This will result in

![Text Content](https://i.imgur.com/RtZECpK.png)

So, you have to add a `{}` object inside the `[]` of a Lesson. The compulsory key is `type` whose value should be `text`.  Following this, add another key `content` whose value should contain the text you want to display in the lesson.

### 2. ** Image **

Adding an image is just as simple like adding a text. Simply use below code to know how

```json
[
    {
        "lesson": [
            {
                "type": "image",
                "content": "https://cdn.pixabay.com/photo/2018/07/28/19/57/lunar-eclipse-3568835_960_720.jpg"
            }
        ]
    },
    ...
```

This will result in

![Image Content](https://i.imgur.com/kAfFA05.png)

So this time, `type` key has value `image` and an additional key is `content` which should contain the url of the image you want to display. If the image doesn't load, try to host the image yourself online somewhere and replace the url in `content`.

### 3. ** Code **

Code snippets can also be added to Lesson which can be copied easily right from the application. Use below code to know how

```json
[
    {
        "lesson": [
            {
                "type": "code",
                "content": "man ls"
            }
        ]
    },
    ...
```

This will result in

![Code Content](https://i.imgur.com/qpwdZVy.png)

Now you might have guessed what `type` we are using. And similar to above convention, add additional key `content` and value should be whatever text you want the user to copy.

### 4. ** Question **

Now this one is a little advance content type that the others. Let's first use the code below and we'll try to understand what is means by looking into how it works

```json
[
    {
        "lesson": [
            {
                "type": "question",
                "content": {
                    "question": "Is Linux a Monolithic Kernal? Answer yes or no.",
                    "answer": "yes",
                    "hint": "do some research online"
                }
            }
        ]
    },
    ...
```

This is how the question will be displayed

![Question Content](https://i.imgur.com/LGckleu.png)

Here you can see we see our question, hint is displayed inside the input box when no input is provided. When we enter an answer and submit it is compared with `answer` key on the JSON and appropriate result will be displayed. `hint` is completely optional.

![Correct Answer](https://i.imgur.com/lx1Wd8R.png)

Also, below is the example of how you can add multiple content to a single lesson. Try it out!

```json
[
    {
        "lesson": [
            {
                "type": "text",
                "content": "This is my AWESOME tutorial. Checkout the image below!"
            },
            {
                "type": "image",
                "content": "https://cdn.pixabay.com/photo/2017/01/16/19/17/horses-1984977_960_720.jpg"
            }
        ]
    },
    {
        "lesson": [
            {
                "type": "text",
                "content": "Another awesome lesson for you!"
            },
            {
                "type": "image",
                "content": "https://cdn.pixabay.com/photo/2014/11/29/09/56/sleigh-ride-549727_960_720.jpg"
            }
        ]
    },
    {
        "lesson": [
            {
                "type": "text",
                "content": "WOW! You made it to the end!"
            },
            {
                "type": "question",
                "content": {
                    "question": "How many horses did you see?",
                    "answer": "4",
                    "hint": "read the tutorial from the start"
                }
            },
            {
                "type": "text",
                "content": "Thank you for visiting my tutorial - Shashank"
            }
        ]
    }
]
```

![Screencast](https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard/uploads/ae5d3653fe39b2243c28234f1e6eace1/finalstep.mp4)

There you go. You can build your Tutorial now. Raise a merge request on the [New Contributor Wizard](https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard) repository with your JSON file so that the world can look at it.
