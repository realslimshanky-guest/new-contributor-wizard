# New Contributor Wizard - User Documentation

### Download the Application

More description on how you can download and install the application from your package manager will ba available soon. Until then you may want to build the application from source yourself, read [Developer's Doc](developer.md) to find out more.
