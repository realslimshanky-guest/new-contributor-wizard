# New Contributor Wizard

### Description

A GSoC 2018 project to help new contributors get started with open source contributions. New Contribution Wizard is a GUI application which on completion will contains all the tutorials and tools to facilitate open source contribution.

### Project Management

Project management is currently underway on [Redmine](https://outreach-lab.debian.net/redmine/projects/new-contributor-wizard) hosted on Debian.

### Prerequisites To Build Application

- GNU/Linux OS (Tested on Debian 9)
- Python (Tested on Python => 3.5)

### Documentation

#### Developer Documentation

These documentations help developers who are willing to build this application from source. Checkout docs from [here](docs/developer.md).

#### Contributor Documentation

These documentations contains guildelines for the contributors get started with best practices to help out in contributing to this project. Checkout docs from [here](docs/contributing.md).

##### Contribute To Tutorials

Contributing Tutorials is as simple as creating a JSON file, NO CODING REQUIRED! You can create a JSON file in order to contribute to Tutorials of any module of your choice. Visit [Contribute To Tutorials](docs/contribute-to-tutorials.md) to know how.

#### User Documentation

These documentations help end users get started with the application and describe the features which can be explored further. Checkout docs from [here](docs/user.md).

#### Changelogs

Changelogs for this project is based on [SemVer](https://semver.org/) or Semantic Versioning. Checkout Changelogs from [here](changelog).

### Contributors

Checkout [CONTRIBUTORS](CONTRIBUTORS.md) to know more about the awesome people behind this project.


